from django.conf.urls import include
from django.urls import path
from django.contrib import admin

urlpatterns = [
    path('admin/', admin.site.urls),
    path('story9/', include('story_9.urls')),
    path('story10/', include('story_10.urls')),
    path('', include('lab_1.urls')),
]
