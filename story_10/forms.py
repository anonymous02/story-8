from django import forms
from .models import Registration

class RegistrationForm(forms.ModelForm):
    name = forms.CharField(required=True, max_length=50, widget=forms.TextInput(attrs={
        'class': 'form-control',
        'onInput': 'cek()',
    }))
    email = forms.EmailField(required=True, max_length=50, widget=forms.EmailInput(attrs={
        'class': 'form-control',
        'onInput': 'cek()',
    }))
    password = forms.CharField(required=True, max_length=50, widget=forms.PasswordInput(attrs={
        'class': 'form-control',
        'onInput': 'cek()',
    }))    
    
    class Meta:        
        model = Registration
        fields = [
            'name',
            'email',
            'password'
        ]