from django.urls import path
from .views import registration_form, check, save, tampil, delete

#url for app
urlpatterns = [
   path('', registration_form, name='registration_form'),
   path('check/', check, name="check"),
   path('save/', save, name="save"),
   path('tampil/', tampil, name="tampil"),
   path('delete/', delete, name="delete"),
]
