function cek(){
    var input = document.getElementById("subscription-form");
    var name = input.elements[1].value;
    var email = input.elements[2].value;
    var password = input.elements[3].value;
    var csrftoken = $('[name=csrfmiddewaretoken]').val();

    $.ajax({
        method: "POST",
        url: "/story10/check/",
        headers: {
            "X-CSRFToken": csrftoken
        },
        data: {
            'name': name,
            'email': email,
            'password': password,
        },
        dataType: "json",
        success: function(result){
            button = document.getElementById("save");
            if(result['is_valid']){
                if(result['is_exist']){
                    button.disabled = true;
                    alert("Email telah pernah digunakan")
                }
                else{
                    button.disabled = false;
                }
            }
            else{
                button.disabled = true;
            }
        }
    });
};

function hapus(id){
    var email = $("#email"+id).html()
    var check = confirm("Are you sure want to delete this data");
    if (check == false) {
        return false;
    }    

    $.ajax({
        url: '/story10/delete/',
        method: 'POST',
        data: {
            'email' : email,
        },
        success: function(response) {
            $("#tampil").click();
        }
    })

}

$(document).ready(function(){
    $("#save").click(function(){
        var input = document.getElementById("subscription-form");
        var name = input.elements[1].value;
        var email = input.elements[2].value;
        var password = input.elements[3].value;
        var csrftoken = $('[name=csrfmiddewaretoken]').val();
        
        $.ajax({
            method: "POST",
            url: "/story10/save/",
            headers: {
                "X-CSRFToken": csrftoken
            },
            data: {
                'name': name,
                'email': email,
                'password': password,
            },
            dataType: "json",
            complete: function(result){
                button = document.getElementById("save");
                button.disabled = true;
                input.reset();
            }
        });
    });

    $("#tampil").click(function(){
        var csrftoken = $('[name=csrfmiddewaretoken]').val();
        $.ajax({
            method: "GET",
            url: "/story10/tampil/",
            headers: {
                "X-CSRFToken": csrftoken
            },
            success: function(result){
                $('td').remove();
                for(var i in result['subscribers']){
                    var data = "<tr>\<td>"+ result['subscribers'][i][0] +"</td><td id='email"+i+"'>"+ result['subscribers'][i][1] +"</td>" + "<td>"+"<Button id=" + i + 
                   " class='btn' onclick='hapus("+i+")'>"+"Hapus"+"</Button>" + "</td>"
                    $('tbody').append(data);
                };
            }
        });
    });
})