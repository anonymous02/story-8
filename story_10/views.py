from django.shortcuts import render
from .forms import RegistrationForm
from .models import Registration
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse, HttpResponse, HttpResponseForbidden
import json

@csrf_exempt
def registration_form(request):
    form = RegistrationForm(request.POST or None)
    response = {
        'registrationform' : form
    }
    return render(request, 'registrationform.html', response)

@csrf_exempt
def check(request):
    if(request.method == "POST"):
        email_exist = Registration.objects.filter(email=request.POST['email']).exists()
        print(email_exist)
        form = RegistrationForm({
            'name': request.POST['name'],
            'email': request.POST['email'],
            'password': request.POST['password']
        })
        print(555)
        return JsonResponse({
            'is_valid': form.is_valid(),
            'is_exist': email_exist,
        })
    else:
        return HttpResponseForbidden()

@csrf_exempt
def save(request):
    if (request.method == "POST"):   
        
        print(444)    
        Registration.objects.create(
            name = request.POST['name'],
            email = request.POST['email'],
            password = request.POST['password']
        )
        return HttpResponse('yeyy')
    else:
        print(333)
        return HttpResponseForbidden()

@csrf_exempt
def tampil(request):
    subscribers = {}
    subscriber = Registration.objects.all()
    for i in range(len(subscriber)):
        subscribers[i] = [subscriber[i].name, subscriber[i].email]

    return JsonResponse({'subscribers': subscribers})

@csrf_exempt
def delete(request):
    if (request.method == 'POST'):    
        subscriber = Registration.objects.filter(email=request.POST['email'])
        subscriber.delete()
        return HttpResponse('dihapus')
    else:
        return HttpResponseForbidden()
