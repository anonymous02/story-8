from django.urls import re_path
from .views import portfolio_form
from .views import profile

#url for app
urlpatterns = [
    re_path(r'^$', portfolio_form, name='portfolio_form'),
    re_path(r'^profile/', profile, name='profile'),
]
