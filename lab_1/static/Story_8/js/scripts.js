$(document).ready(function(){
    
    myFunction();
    
    let bg = ['black', 'white'];
    let txt = ['white', 'black'];
    let i = 0;

    $("#dark").click(function(){
        i = 0;
        $("body").css("background-color", bg[i]);
        $("body").css("transition","1s");
        $("h3").css("color", txt[i]);
    });

    $("#light").click(function(){
        i = 1;
        $("body").css("background-color", bg[i]);
        $("body").css("transition","1s");
        $("h3").css("color", txt[i]);
    });

    $("#dark").mouseenter(function(){
        $("body").css("background-color", bg[0]);
        $("body").css("transition","1s");
        $("h3").css("color", txt[0]);
    });

    $("#light").mouseenter(function(){
        $("body").css("background-color", bg[1]);
        $("body").css("transition","1s");
        $("h3").css("color", txt[1]);
    });

    $("#dark").mouseleave(function(){
        $("body").css("background-color", bg[i]);
        $("body").css("transition","1s");
        $("h3").css("color", txt[i]);
    });

    $("#light").mouseleave(function(){
        $("body").css("background-color", bg[i]);
        $("body").css("transition","1s");
        $("h3").css("color", txt[i]);
    });

    $("#accordion").accordion();
});

var myVar;

function myFunction() {
  myVar = setTimeout(showPage, 3000);
}

function showPage() {
  document.getElementById("loader").style.display = "none";
  document.getElementById("isi").style.display = "block";
  $("#accordion").css("visibility","visible");
}