from django.urls import path
from .views import index, laptop_info

#url for app
urlpatterns = [
    path('', index, name='index'),
    path('info/', laptop_info, name="laptop_info"),
]
