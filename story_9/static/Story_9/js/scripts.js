$(document).ready(function(){
    paginator();
});

var harga = 0;

function laptop_info(start){
    var csrftoken = $('[name=csrfmiddewaretoken]').val();
    $.ajax({
        method: 'GET',
        url: '/story9/info/',
        headers: {
            "X-CSRFToken": csrftoken,
        },
        success: function(result) {
            for(i=start*10; i<start*10+10; i++){
                var data = "<tr>\<td>"+(i+1)+"</td><td>"+ result['list'][i].id +"</td><td>"+ result['list'][i].name +"</td><td>"+ 
                           result['list'][i].detail +"</td><td id="+ (i+1) +" >"+ result['list'][i].price +"</td>"+ "<td>"+"<Button id=" + result['list'][i].id + 
                           " class='btn' value="+(i+1)+">"+result['list'][i].button+"</Button>" + "</td>" +"</tr>"
                $('tbody').append(data);
            };
            button(result['list']);
        },
    });
};


function button(result){
    $("button").click(function(){
        console.log();
        if($(this).text() == "Add to wishlist"){
            $(this).text("Remove from wishlist");
            result[Number($(this).attr("value")-1)]['button'] = 'Remove from wishlist';
            console.log(result[Number($(this).attr("value")-1)]['button']);
            harga = harga + Number($("#"+$(this).attr("value")).text());
            $("#harga").text(harga);
        }
        else if($(this).text() == "Remove from wishlist"){
            $(this).text("Add to wishlist");
            result[Number($(this).attr("value")-1)]['button'] = 'Add to wishlist';

            harga = harga - Number($("#"+$(this).attr("value")).text());
            $("#harga").text(harga);
        }
        else{
            $('td').remove();
            laptop_info(Number($(this).text())-1);
        }
    });
};

function paginator(){
    for(i=1;i<=9;i++){
        let button = "<Button>"+(i)+"</Button>";
        $('#paginations').append(button);
    };
};