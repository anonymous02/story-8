from django.shortcuts import render
import json
import requests
from django.http import  JsonResponse, HttpResponseRedirect

def index(request):
    return render(request, 'index.html')

def laptop_info(request):
    laptops = requests.get("https://enterkomputer.com/api/product/notebook.json").json()

    res = 0
    laptop_list = []
    for laptop in laptops:
        new_laptops = { 'id' : '', 'name' : '', 'detail' : '', 'price' : '', 'button' : ''}

        if 'i7-8750' in laptop['details']:
            new_laptops['id'] = laptop['id']
            new_laptops['name'] = laptop['name']
            new_laptops['detail'] = laptop['details']
            new_laptops['price'] = laptop['price']
            new_laptops['button'] = 'Add to wishlist'
            laptop_list.append(new_laptops)

    return JsonResponse({'list' : laptop_list})