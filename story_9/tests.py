from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
import unittest
from .views import index, laptop_info
import requests

# Create your tests here.
class Story9Unittest(TestCase):
    def test_story_url_exist(self):
        response = Client().get('/story9/')
        self.assertEqual(response.status_code,200)

    def test_json_able_to_post_to_url(self):
        response = Client().get('/story9/info/')
        self.assertEqual(response.status_code,200)